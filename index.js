const rect = require('./rectangle'),
      express = require('express'),
      http  = require('http'),
      morgan = require('morgan'),
      fs = require('fs'),
      path = require('path'),
      bodyParser = require('body-parser')

const dishRouter = require('./routes/dishRouter')

const hostname = 'localhost'
const port = 3000

const app = express()

app.use(bodyParser.json())
app.use(morgan('dev'))

app.use('/dishes', dishRouter)

app.use(express.static(__dirname + '/public'))

const server = http.createServer(app)

server.listen(port, hostname, () =>{
    console.log(`Server running on https://${hostname}:${port}/`)
})  

